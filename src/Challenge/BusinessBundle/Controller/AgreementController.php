<?php

namespace Challenge\BusinessBundle\Controller;

use Challenge\BusinessBundle\Services\Impl\PartnershipImpl;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Challenge\BusinessBundle\Services\AgreementDAO;

class AgreementController extends Controller
{

    /**
     * @Route("/agreement", name="agreement_create_view", options={"expose"=true})
     * @Template()
     * @Method({"GET"})
     */
    public function newAction()
    {
        return array();
    }


    /**
     * @Route("/agreement/new", name="agreement_create", options={"expose"=true})
     * @Template("::base.json.twig")
     * @Method({"POST"})
     */
    public function createAgreementAction()
    {
        //TODO  + ruta sin /new

        $sourceBusinessId = $this->get('request')->request->get('sourceBusiness');
        $relatedBusinessId = $this->get('request')->request->get('relatedBusiness');
        $impliedBusinessesIds = $this->get('request')->request->get('impliedBusinesses');


        /** @var AgreementDAO $agreementDAO */
        $agreementDAO = $this->get('challenge_business.agreement');

        $success = $agreementDAO->create($sourceBusinessId, $relatedBusinessId, $impliedBusinessesIds);

        if($success){
            return array('data' => 'ok');
        } else {
            return array('data' => 'ko');
        }

    }

}