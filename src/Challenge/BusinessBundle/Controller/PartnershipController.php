<?php

namespace Challenge\BusinessBundle\Controller;

use Challenge\BusinessBundle\Services\Impl\PartnershipImpl;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Challenge\BusinessBundle\Services\PartnershipDAO;

class PartnershipController extends Controller
{


    /**
     * @Route("/partnership/new", name="partnership_create", options={"expose"=true})
     * @Template("::base.json.twig")
     * @Method({"POST"})
     */
    public function createPartnershipAction()
    {
        //TODO  + ruta sin /new

        $SourceBusinessId = $this->get('request')->request->get('sourceBusiness');
        $DestinationBusinessId = $this->get('request')->request->get('destinationBusiness');
        $type = $this->get('request')->request->get('partnershipType');

        /** @var PartnershipDAO $partnershipDAO */
        $partnershipDAO = $this->get('challenge_business.partnership');

        $success = $partnershipDAO->create($SourceBusinessId, $DestinationBusinessId, $type);

        if($success){
            return array('data' => 'ok');
        } else {
            return array('data' => 'ko');
        }

    }

    /**
     * @Route("/partnership", name="partnership_create_view", options={"expose"=true})
     * @Template()
     * @Method({"GET"})
     */
    public function newAction()
    {
        return array();
    }


    /**
     * @Route("/partnership/types.json", name="partnership_types_json", options={"expose"=true})
     * @Template("::base.json.twig")
     *
     */
    public function partneshipTypesJSONAction()
    {
        $availableTypes = PartnershipImpl::getAvailableTypes();
        return array('data' => $availableTypes);
    }

}
