<?php

namespace Challenge\BusinessBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Challenge\BusinessBundle\Services\BusinessDAO;
use Challenge\BusinessBundle\Services\PartnershipDAO;
use Challenge\BusinessBundle\Services\AgreementDAO;

class BusinessController extends Controller
{


    /**
     * @Route("/business/new", name="business_create", options={"expose"=true})
     * @Template("::base.json.twig")
     *
     */
    public function createBusinessAction()
    {
        //TODO @Method({"POST"}) + ruta sin /new

        $businessName = $this->get('request')->request->get('name');

        /** @var BusinessDAO $businessDAO */
        $businessDAO = $this->get('challenge_business.business');

        $success = $businessDAO->create($businessName);

        if($success){
            return array('data' => 'ok');
        } else {
            return array('data' => 'ko');
        }

    }

    /**
     * @Route("/business.json", name="business_json", options={"expose"=true})
     * @Template("::base.json.twig")
     *
     */
    public function businessJSONAction()
    {
        /** @var BusinessDAO $businessDAO */
        $businessDAO = $this->get('challenge_business.business');

        $business = $businessDAO->findAll();

        $return = array();

        foreach($business as $b) {
            $return[$b->getName()] = $b->getId();
        }

        return array('data' => $return);
    }

    /**
     * @Route("/business")
     * @Template()
     * @Method({"GET"})
     */
    public function newAction()
    {
        /**
         * Listado de las entidades Business + form creación
         */

        /** @var BusinessDAO $businessDAO */
        $businessDAO = $this->get('challenge_business.business');

        $businesses = $businessDAO->findAll();
        return array("businesses" => $businesses);
    }

    /**
     * @Route("/business/{id}", name="business_view", options={"expose"=true})
     * @Template()
     *
     */
    public function viewAction($id)
    {
        /** @var BusinessDAO $businessDAO */
        $businessDAO = $this->get('challenge_business.business');

        $request = $this->getRequest();

        $filter = $request->query->get('filter');
        $order = $request->query->get('order');

        $business = $businessDAO->find($id);

        if($business) {
            /** @var PartnershipDAO $businessDAO */
            $partnershipDAO = $this->get('challenge_business.partnership');
            $partnerships = $partnershipDAO->findAllForBusinessFilteredAndOrdered($business, $filter, $order);

            /** @var AgreementDAO $businessDAO */
            $agreementDAO = $this->get('challenge_business.agreement');
            $agreements = $agreementDAO->findAllForBusiness($business);

            return array(
                "name" => $business->getName(),
                "filter" => $filter,
                "order" => $order,
                "partnerships" => $partnerships,
                "agreements" => $agreements,
            );
        } else {
            throw new NotFoundHttpException("Page not found");
        }



    }

    /**
     * @Route("/business/{id}/partners.json", name="business_partners_json", options={"expose"=true})
     * @Template("::base.json.twig")
     *
     */
    public function partnersJSONAction($id)
    {
        /** @var BusinessDAO $businessDAO */
        $businessDAO = $this->get('challenge_business.business');
        $business = $businessDAO->find($id);

        if($business) {
            /** @var PartnershipDAO $businessDAO */
            $partnershipDAO = $this->get('challenge_business.partnership');
            $partnerships = $partnershipDAO->findAllForBusinessFilteredAndOrdered($business, null, null);

            $return = array();

            foreach($partnerships as $partnership) {
                $return[$partnership->getDestinationBusiness()->getName()] = $partnership->getDestinationBusiness()->getId();
            }

            return array('data' => $return);

        } else {
            throw new NotFoundHttpException("Page not found");
        }
    }
}
