<?php

namespace Challenge\BusinessBundle\Services;

use Challenge\BusinessBundle\Entity\Business;

interface PartnershipDAO
{
    public function find($id);

    public function create($sourceBusinessId, $destinationBusinessId, $type);

    public function findAllForBusinessFilteredAndOrdered(Business $sourceBusiness, $filter, $order);
}