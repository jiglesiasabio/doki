<?php

namespace Challenge\BusinessBundle\Services;

use Challenge\BusinessBundle\Entity\Business;

interface AgreementDAO
{
    public function find($id);

    public function create($sourceBusinessId, $destinationBusinessId, $impliedBusinesses);

    public function findAllForBusiness(Business $business);

}