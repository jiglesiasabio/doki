<?php

namespace Challenge\BusinessBundle\Services;

interface BusinessDAO
{
    public function find($id);

    public function create($name);

    public function findAll();
}