<?php
namespace Challenge\BusinessBundle\Services\Impl;

use Challenge\BusinessBundle\Entity\Business;
use Challenge\BusinessBundle\Services\BusinessDAO;
use Doctrine\ORM\EntityManager;

class BusinessImpl implements BusinessDAO
{

    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function find($id)
    {
        $repository = $this->entityManager->getRepository('ChallengeBusinessBundle:Business');
        $business = $repository->findOneBy(array('id' => $id));
        return $business;
    }

    public function create($name)
    {
        $business = new Business();
        $business->setName($name);

        try {
            $this->entityManager->persist($business);
            $this->entityManager->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            return false;
        }

        return $business;
    }

    public function findAll()
    {
        $repository = $this->entityManager->getRepository('ChallengeBusinessBundle:Business');
        $business = $repository->findAll();
        return $business;
    }

}