<?php
namespace Challenge\BusinessBundle\Services\Impl;

use Challenge\BusinessBundle\Entity\Partnership;
use Challenge\BusinessBundle\Services\PartnershipDAO;
use Challenge\BusinessBundle\Services\AgreementDAO;
use Challenge\BusinessBundle\Services\BusinessDAO;

use Challenge\BusinessBundle\Entity\Business;
use Challenge\BusinessBundle\Entity\Agreement;
use Challenge\BusinessBundle\Entity\AgreementLink;


use Doctrine\ORM\EntityManager;

class AgreementImpl implements AgreementDAO
{

    private $entityManager;
    private $businessDAO;

    public function __construct(EntityManager $entityManager, BusinessDAO $businessDAO)
    {
        $this->entityManager = $entityManager;
        $this->businessDAO = $businessDAO;
    }


    public function find($id) {
        $repository = $this->entityManager->getRepository('ChallengeBusinessBundle:Agreement');
        $agreement = $repository->findOneBy(array('id' => $id));
        return $agreement;
    }

    public function create($sourceBusinessId, $relatedBusinessId, $impliedBusinesses) {

        $sourceBusiness = $this->businessDAO->find($sourceBusinessId);
        $relatedBusiness = $this->businessDAO->find($relatedBusinessId);

        if($sourceBusiness && $relatedBusiness) {

            /** @var Agreement $agreement */
            $agreement = new Agreement();

            $agreement->setSourceBusiness($sourceBusiness);
            $agreement->setRelatedBusiness($relatedBusiness);

            $this->entityManager->persist($agreement);

            $position = 0;
            foreach($impliedBusinesses as $impliedBusinessId) {
                $impliedBusiness = $this->businessDAO->find($impliedBusinessId);

                if($impliedBusiness) {
                    $agreementLink = new AgreementLink();
                    $agreementLink->setBusiness($impliedBusiness);
                    $agreementLink->setPosition($position);
                    $agreementLink->setAgreement($agreement);
                    $this->entityManager->persist($agreementLink);
                    $position++;
                }
            }

            $this->entityManager->flush();

            return $agreement;

        }
        return false;
    }


    public function findAllForBusiness(Business $business){

        $result = Array();

        /* Entity 'Agreement' */
        $query = $this->entityManager->createQuery(
            'SELECT a
            FROM ChallengeBusinessBundle:Agreement a
            WHERE a.sourceBusiness = :sourceBusiness
            ORDER BY a.id DESC'
        );

        $query->setParameters(array(
            'sourceBusiness' => $business,
        ));

        $agreements = $query->getResult();

        foreach($agreements as $agreement) {

            //Fetch all the links for each agreement

            $query = $this->entityManager->createQuery(
                'SELECT al
                FROM ChallengeBusinessBundle:AgreementLink al
                WHERE al.agreement = :agreement
                ORDER BY al.position ASC'
            );

            $query->setParameters(array(
                'agreement' => $agreement,
            ));

            $agreementLinks = $query->getResult();

            $result[] = Array(
                "agreement" => $agreement,
                "agreementLinks" => $agreementLinks
            );

        }

        return $result;
    }

}