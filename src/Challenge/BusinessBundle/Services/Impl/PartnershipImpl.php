<?php
namespace Challenge\BusinessBundle\Services\Impl;

use Challenge\BusinessBundle\Entity\Partnership;
use Challenge\BusinessBundle\Services\PartnershipDAO;
use Challenge\BusinessBundle\Services\BusinessDAO;

use Challenge\BusinessBundle\Entity\Business;

use Doctrine\ORM\EntityManager;

class PartnershipImpl implements PartnershipDAO
{

    private $entityManager;
    private $businessDAO;

    public function __construct(EntityManager $entityManager, BusinessDAO $businessDAO)
    {
        $this->entityManager = $entityManager;
        $this->businessDAO = $businessDAO;
    }

    public static function getAvailableTypes()
    {
        return array('client', 'provider');
    }

    public function find($id)
    {
        $repository = $this->entityManager->getRepository('ChallengeBusinessBundle:Partnership');
        $business = $repository->findOneBy(array('id' => $id));
        return $business;
    }

    public function create($sourceBusinessId, $destinationBusinessId, $type)
    {

        $sourceBusiness = $this->businessDAO->find($sourceBusinessId);
        $destinationBusiness = $this->businessDAO->find($destinationBusinessId);

        if ($sourceBusiness && $destinationBusiness && ($sourceBusiness != $destinationBusiness) && in_array($type, $this->getAvailableTypes())) {
            $partnership = new Partnership();

            $partnership->setSourceBusiness($sourceBusiness);
            $partnership->setDestinationBusiness($destinationBusiness);
            $partnership->setType($type);

            try {
                $this->entityManager->persist($partnership);
                $this->entityManager->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                return false;
            }

            return $partnership;
        }

        return false;

    }


    public function findAllForBusinessFilteredAndOrdered(Business $sourceBusiness, $filter, $order)
    {

        $queryString = "SELECT p
                FROM ChallengeBusinessBundle:Partnership p JOIN p.destinationBusiness db
                WHERE p.sourceBusiness = :sourceBusiness ";

        if ($filter == 'clients'){
            $queryString = $queryString . "AND p.type = 'client' ";
        } else if ($filter == 'providers') {
            $queryString = $queryString . "AND p.type = 'provider' ";
        }

        if($order == "name"){
            $queryString = $queryString . "ORDER BY db.name DESC ";
        } else if ($order == "type") {
            $queryString = $queryString . "ORDER BY p.type DESC ";
        }

        $query = $this->entityManager->createQuery($queryString);

        $query->setParameters(array(
            'sourceBusiness' => $sourceBusiness,
        ));

        $partnerships = $query->getResult();
        return $partnerships;
    }

}