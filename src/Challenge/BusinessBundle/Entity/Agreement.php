<?php

namespace Challenge\BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Agreement
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Agreement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\Challenge\BusinessBundle\Entity\Business")
     * @ORM\JoinColumn(name="source_business_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $sourceBusiness;

    /**
     * @ORM\ManyToOne(targetEntity="\Challenge\BusinessBundle\Entity\Business")
     * @ORM\JoinColumn(name="related_business_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $relatedBusiness;

    /**
     * @param mixed $relatedBusiness
     */
    public function setRelatedBusiness($relatedBusiness)
    {
        $this->relatedBusiness = $relatedBusiness;
    }

    /**
     * @return mixed
     */
    public function getRelatedBusiness()
    {
        return $this->relatedBusiness;
    }

    /**
     * @param mixed $sourceBusiness
     */
    public function setSourceBusiness($sourceBusiness)
    {
        $this->sourceBusiness = $sourceBusiness;
    }

    /**
     * @return mixed
     */
    public function getSourceBusiness()
    {
        return $this->sourceBusiness;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
