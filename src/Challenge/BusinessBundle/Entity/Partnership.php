<?php

namespace Challenge\BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Partnership
 *
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="partnership_unique", columns={"source_business_id", "destination_business_id", "type"})})

 * @ORM\Entity
 */
class Partnership
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\Challenge\BusinessBundle\Entity\Business")
     * @ORM\JoinColumn(name="source_business_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $sourceBusiness;

    /**
     * @ORM\ManyToOne(targetEntity="\Challenge\BusinessBundle\Entity\Business")
     * @ORM\JoinColumn(name="destination_business_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $destinationBusiness;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, unique=false)
     */
    private $type;

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $destinationBusiness
     */
    public function setDestinationBusiness($destinationBusiness)
    {
        $this->destinationBusiness = $destinationBusiness;
    }

    /**
     * @return mixed
     */
    public function getDestinationBusiness()
    {
        return $this->destinationBusiness;
    }

    /**
     * @param mixed $sourceBusiness
     */
    public function setSourceBusiness($sourceBusiness)
    {
        $this->sourceBusiness = $sourceBusiness;
    }

    /**
     * @return mixed
     */
    public function getSourceBusiness()
    {
        return $this->sourceBusiness;
    }
}
