<?php

namespace Challenge\BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AgreementLink
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class AgreementLink
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\Challenge\BusinessBundle\Entity\Agreement")
     * @ORM\JoinColumn(name="agreement_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $agreement;

    /**
     * @ORM\ManyToOne(targetEntity="\Challenge\BusinessBundle\Entity\Business")
     * @ORM\JoinColumn(name="business_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $business;

    /**
     * @var integer
     * @ORM\Column(name="position", type="integer")
     */
    protected $position;

    /**
     * @param mixed $business
     */
    public function setBusiness($business)
    {
        $this->business = $business;
    }

    /**
     * @return mixed
     */
    public function getBusiness()
    {
        return $this->business;
    }

    /**
     * @param mixed $agreement
     */
    public function setAgreement($agreement)
    {
        $this->agreement = $agreement;
    }

    /**
     * @return mixed
     */
    public function getAgreement()
    {
        return $this->agreement;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
