$(document).ready(function () {
    console.log("business.js ready!");

    $(document).on('click', '#newBusinessBtn', function () {

        var name = $("#name").val();

        if (name) {
            $.post(Routing.generate('business_create'), {name: name}, function (data) {
                console.log(data);

                var jsonData = $.parseJSON(data);

                if (jsonData === 'ko') {
                    alert("Error - duplciated business");
                } else if (jsonData === 'ok') {
                    alert("Business created!");
                    $("#name").html('');
                }
            });
        }
    });


});