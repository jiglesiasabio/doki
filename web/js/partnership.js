$(document).ready(function () {
    console.log("partnership.js ready!");

    populateBusinessSelects();
    populateTypeSelect();

    $(document).on('click', '#newPartnershipBtn', function () {

        createPartnership();

    });

    function populateBusinessSelects() {
        /* Populate the selects with the available business */
        $.get(Routing.generate('business_json'), function (data) {
            var jsonData = $.parseJSON(data);

            console.log(jsonData);

            for (var key in jsonData) {
                if (jsonData.hasOwnProperty(key)) {

                    addBusinessToSelectInputs($("#sourceBusiness"), jsonData[key], key);
                    addBusinessToSelectInputs($("#destinationBusiness"), jsonData[key], key);
                }
            }
        });
    }

    function populateTypeSelect() {
        /* Populate the "type" select */
        $.get(Routing.generate('partnership_types_json'), function (data) {
            var jsonData = $.parseJSON(data);
            console.log(jsonData);

            for (var key in jsonData) {
                if (jsonData.hasOwnProperty(key)) {
                    addBusinessToSelectInputs($("#partnershipType"), jsonData[key], jsonData[key]);
                }
            }
        });
    }

    function addBusinessToSelectInputs(selector, key, text) {
        selector
            .append($("<option></option>")
                .attr("value", key)
                .text(text));
    }

    function createPartnership() {
        var sourceBusiness = $("#sourceBusiness").val();
        var destinationBusiness = $("#destinationBusiness").val();
        var partnershipType = $("#partnershipType").val();

        if (sourceBusiness && destinationBusiness && partnershipType) {

            if(sourceBusiness != destinationBusiness){
                var postData = {
                    sourceBusiness: sourceBusiness,
                    destinationBusiness: destinationBusiness,
                    partnershipType: partnershipType
                };

                $.post(Routing.generate('partnership_create'), postData, function (data) {
                    console.log(data);

                    var jsonData = $.parseJSON(data);

                    if (jsonData === 'ko') {
                        alert("Error - duplicated partnership");
                    } else if (jsonData === 'ok') {
                        alert("Partnership created!");
                    }
                });
            } else {
                alert("Error - source and destination are the same business!!");
            }
        }
    }


});
