$(document).ready(function () {
    console.log("agreement.js ready!");

    var impliedBusinesses = new Array();
    var sourceBusiness, relatedBusiness;

    populateSourceBusinessSelect();

    $(document).on('change', '#sourceBusiness', function () {

        populateRelatedAndImpliedBusinessSelect($('#sourceBusiness').val());
        sourceBusiness = $('#sourceBusiness').val();

    });

    $(document).on('change', '#relatedBusiness', function () {

        relatedBusiness = $('#relatedBusiness').val();
        populateImpliedBusinessSelect(relatedBusiness);

    });


    /* ADD IMPLIED BUSINESS */

    $(document).on('click', '#addImpliedBusinessBtn', function () {

        var impliedBusinessId = $("#impliedBusiness").val();
        var impliedBusinessText = $("#impliedBusiness option:selected").text();

        if (impliedBusiness) {

            //Add the new implied business to the array
            impliedBusinesses.push(impliedBusinessId);

            //Show it in the UI
            $('#impliedBusinessesList').append("<li>" + impliedBusinessText + "</li>");

            //Update the select with the partners of this new implied business
            populateImpliedBusinessSelect(impliedBusinessId);

        }
    });


    /* SAVE BUTTON */

    $(document).on('click', '#newAgreementBtn', function () {

        relatedBusiness = $('#relatedBusiness').val();
        sourceBusiness = $('#sourceBusiness').val();

        if (sourceBusiness && relatedBusiness) {
            var postData = {
                "sourceBusiness": sourceBusiness,
                "relatedBusiness": relatedBusiness,
                "impliedBusinesses": impliedBusinesses
            };

            $.post(Routing.generate('agreement_create'), postData, function (data) {
                console.log(data);

                var jsonData = $.parseJSON(data);

                if (jsonData === 'ko') {
                    alert("Error creating agreement");
                } else if (jsonData === 'ok') {
                    alert("Agreement created!");
                }
            });
        } else {
            alert("Error!");
        }

    });


    function populateSourceBusinessSelect() {
        /* Populate the select with the available business */
        $.get(Routing.generate('business_json'), function (data) {
            var jsonData = $.parseJSON(data);

            console.log(jsonData);

            for (var key in jsonData) {
                if (jsonData.hasOwnProperty(key)) {
                    addBusinessToSelectInputs($("#sourceBusiness"), jsonData[key], key);
                }
            }

            populateRelatedAndImpliedBusinessSelect($('#sourceBusiness').val());
        });
    }

    function populateRelatedAndImpliedBusinessSelect(id) {
        $('#relatedBusiness').html('');
        $('#impliedBusiness').html('');

        /* Populate the select with the available business */
        $.get(Routing.generate('business_partners_json', {"id": id}), function (data) {
            var jsonData = $.parseJSON(data);

            console.log(jsonData);

            if (!JSONIsEmpty(jsonData)) {
                for (var key in jsonData) {
                    if (jsonData.hasOwnProperty(key)) {
                        addBusinessToSelectInputs($("#relatedBusiness"), jsonData[key], key);
                        addBusinessToSelectInputs($("#impliedBusiness"), jsonData[key], key);
                    }
                }
                $('#relatedBusinessBlock').show(200);
                $('#impliedBusinessBlock').show(200);
            } else {
                alert("This business doesn't have any partners!");
                $('#relatedBusiness').html('');
                $('#impliedBusiness').html('');
                $('#relatedBusinessBlock').hide(200);
                $('#impliedBusinessBlock').hide(200);
            }


        });
    }

    function populateImpliedBusinessSelect(id) {
        $('#impliedBusiness').html('');

        /* Populate the select with the available business */
        $.get(Routing.generate('business_partners_json', {"id": id}), function (data) {
            var jsonData = $.parseJSON(data);

            console.log(jsonData);

            if (!JSONIsEmpty(jsonData)) {

                for (var key in jsonData) {
                    if (jsonData.hasOwnProperty(key)) {
                        addBusinessToSelectInputs($("#impliedBusiness"), jsonData[key], key);
                    }
                }
                $('#impliedBusinessBlock').show(200);
            } else {
                alert("This business doesn't have any partners!");
            }

        });
    }

    function JSONIsEmpty(obj) {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop))
                return false;
        }

        return true;
    }

    function addBusinessToSelectInputs(selector, key, text) {
        selector
            .append($("<option></option>")
                .attr("value", key)
                .text(text));
    }

});